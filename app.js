this.Demos = (function(exports) {

  /**
   * An entry in the demo registry. Contains information
   * about a demo's requirements and how to load it.
   * @interface
   */
  function DemoInfo() {}
  /**
   * Callback fired when the demo should load.
   * @return {Promise} promise that resolves to a demo object
   */
  DemoInfo.prototype.factory = function() {};

  /**
   * Callback fired when the user enters the gallery.
   * Can be used to activate objects that require user
   * interaction.
   */
  DemoInfo.prototype.touchstart = function() {};

  /**
   * URL to a preview image for the demo. Shown in
   * a thumbnail in the gallery.
   * @type {String}
   */
  DemoInfo.prototype.preview = '';

  /**
   * A short, url-safe name for the experiment.
   * @type {String}
   */
  DemoInfo.prototype.slug = '';

  /**
   * Set to true if the demo runs in iOS.
   * @type {Boolean}
   */
  DemoInfo.prototype.iOS = true;

  /**
   * @type {Array.<DemoInfo>}
   */
  var registry = [];
  var apps = [];

  var canvas, glCtx, audioCtx, current,
    gallery, demos, fullyLoaded;

  var noSleepTimer, noSleepVideo;
  if(has.Android) {
    noSleepVideo = document.createElement('video');
	
	// loop the video
	noSleepVideo.addEventListener('ended', function(ev) {
	  noSleepVideo.play();
	});
	
	// start the video on the first touch
	var triggerNoSleep = function() {
	  enableNoSleep();
	  window.removeEventListener('touchstart', triggerNoSleep, false);
	}
    window.addEventListener('touchstart', triggerNoSleep, false);
	
  }

  var options = {
    deviceOrientation: false
  };

  var pt = Date.now();


  var nativePixelRatio = window.devicePixelRatio = window.devicePixelRatio ||
  Math.round(window.screen.availWidth / document.documentElement.clientWidth);

  var devicePixelRatio = nativePixelRatio;

  var optimizer = new FpsOptimizer({
    target: 60,
    good: function() {
      var scale = 1 + (1-(optimizer._iterations/10)) * 0.1;
      devicePixelRatio *= scale;
      if (devicePixelRatio >= nativePixelRatio) {
        devicePixelRatio = nativePixelRatio;
        resize();
        optimizer.complete = true;
        return;
      }
      resize();
    },
    bad: function() {
      var scale = 1 - (1-(optimizer._iterations/10)) * 0.1;
      devicePixelRatio *= scale;
      if (devicePixelRatio <= 0.5) {
        devicePixelRatio = 0.5;
        resize();
        optimizer.complete = true;
        return;
      }
      resize();
    },
    maxIterations: 10
  });

  //window.optimizer = optimizer;

  /**
   * call render on any current application in a RAF
   */
  function loop(ts) {
    if (current) {
      //optimizer.begin();
      if (window.stats) {window.stats.begin();}
      if (current.app.render(ts, ts - pt) === false) {
        history.go(-1);
      }
      //optimizer.end();
      if (window.stats) {window.stats.end();}
    }
    pt = ts;
    if (!loop.stop) {
      requestAnimationFrame(loop);
    }
  }

  /* init audio context */
  function initAudio() {
    var AudioContext = window.AudioContext || window.webkitAudioContext;
    audioCtx = new AudioContext();
    var buffer = audioCtx.createBuffer(1, 1, 22050);
    var source = audioCtx.createBufferSource();
    source.buffer = buffer;
    source.connect(audioCtx.destination);
    source.start(0);
  }

  /* init gl context */
  function initGL() {
    canvas = document.createElement('canvas');
    canvas.style.position = 'fixed';
    canvas.style.top = '0';
    canvas.style.left = '0';
    canvas.style.right = '0';
    canvas.style.bottom = '0';
    canvas.style.width = '100%';
    canvas.style.height = '100%';
    canvas.style.display = 'block';
    resize();
    document.body.appendChild(canvas);

    var attributes = {
      alpha: false,
      depth: true,
      stencil: false,
      antialias: true,
      premultipliedAlpha: true,
      preserveDrawingBuffer: false
    };

    var types = ['webgl', 'experimental-webgl'];
    for (var i = 0; i < types.length; i++) {
      glCtx = canvas.getContext(types[i], attributes);
      if (glCtx) {
        break;
      }
    }

    // clear and reset the gl context
    glCtx.clearColor(0, 0, 0, 1.0);
    glCtx.enable(glCtx.DEPTH_TEST);
    glCtx.depthFunc(glCtx.LEQUAL);
    glCtx.clear(glCtx.COLOR_BUFFER_BIT);
  }

  function isFullscreen() {
    return !(!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement);
  }

  function enterFullscreen() {
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.msRequestFullscreen) {
      document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  }

  function onFullscreenChange() {
    if (!isFullscreen()) {
      exports.close();
    }
  }

  var fullscreenEvents = ['webkitfullscreenchange', 'mozfullscreenchange', 'fullscreenchange', 'MSFullscreenChange'];
  fullscreenEvents.forEach(function(name) {
    document.addEventListener(name, onFullscreenChange, false);
  });

  /**
   * load the demos asyncronously and init the gallery
   */
  function loadAll() {
    initAudio();
    initGL();

    var generators = registry;
    if (_.isFunction(exports._filter)) {
      generators = _.filter(generators, exports._filter);
    }

    if (has.iOS) {
      generators = _.filter(generators, function(x) {
        return (x.iOS !== false);
      });
    }

    var galleryGenerator = _.findWhere(generators, {
      slug: 'gallery'
    });

    generators = _.filter(generators, function(a) {
      return a.slug !== 'gallery';
    });

    function generate(item) {
      return item.factory(glCtx, audioCtx, options).then(function(app) {
        return {
          app: app,
          slug: item.slug,
          row: item.row,
          preview: item.preview,
        };
      });
    }

    var galleryPromise;
    if (galleryGenerator) {
      galleryPromise = generate(galleryGenerator);
    } else {
      galleryPromise = Promise.resolve();
    }

    for (var i = 0; i < generators.length; i++) {
      var generator = generators[i];
      if (generator.touchstart) {
        generator.touchstart();
      }
    }

    /** @type {GalleryLoader} */
    var loader_ = null;

    return GalleryLoader.create(
      './images/loader_sm.gif'
    ).then(function(loader){
      document.body.appendChild(loader.container);
      loader_ = loader;
      loader_.show();
    }).then(function() {
      return galleryPromise;
    }).then(function(_gallery) {
      if (_gallery) {
        gallery = _gallery;
        apps = [gallery];
        resize();
        load(gallery);
      }
      var promises = generators.map(generate);

      var resolvedCount = 0;
      promises = promises.map(function(p) {
        return p.then(function(demo) {
          resolvedCount++;
          loader_.setProgress(resolvedCount, generators.length);

          return demo;
        });
      });

      return Promise.all(promises).then(function(results) {
        apps = apps.concat(results);
        demos = results;
        if (gallery) {
          gallery.app.addDemos(demos);
        }
        resize();
        propagateOrientation();
        fullyLoaded = true;
        if (has.mobile) {
          enableNoSleep();
        }
      });
    }).then(function() {
      loader_.hide();
    }, function(err) {
      console.error(err.stack || err);
      loader_.setMessage('Uh oh, looks like there was an error. Please reload to retry.');
    });

  }

  /**
   * Add cross-browser functionality to keep a mobile device from auto-locking.
   */
  function enableNoSleep(duration) {
    if (has.iOS) {
      disableNoSleep();
      noSleepTimer = setInterval(function() {
        window.location = window.location;
        setTimeout(window.stop, 0);
      }, duration || 30000);
    } else if (has.Android) {
      noSleepVideo.src = '/videos/no-sleep.webm';
      noSleepVideo.play();
    }

  }

  /**
   * Turn off cross-browser functionality to keep a mobile device from
   * auto-locking.
   */
  function disableNoSleep() {
    if (has.iOS) {
      if (noSleepTimer) {
        clearInterval(noSleepTimer);
        noSleepTimer = null;
      }
    } else if (has.Android) {
      noSleepVideo.pause();
      noSleepVideo.src = '';
    }
  }

  /* vibrate if available */
  (function() {
    if (navigator && navigator.vibrate) {
      return;
    }
    window.navigator = window.navigator || {};
    window.navigator.vibrate = function() {};
  })();

  /* unified orientation */
  function getOrientation() {
    switch (window.screen.orientation || window.screen.mozOrientation) {
      case 'landscape-primary':
        return 90;
      case 'landscape-secondary':
        return -90;
      case 'portrait-secondary':
        return 180;
      case 'portrait-primary':
        return 0;
    }
    return window.orientation;
  }

  function propagateOrientation() {
    var orientation = getOrientation();
    apps.forEach(function(item) {
      if (_.isFunction(item.app.orientationchange)) {
        item.app.orientationchange(orientation);
      }
    });
  }

  function onScreenOrientationChangeEvent() {
    var orientation = getOrientation();
    switch (orientation) {
      case -90:
      case 90: // horizontal
        navigator.vibrate(50);
        break;
      case 0: // vertical
      case 180:
        if (current && current !== gallery) {
          history.go(-1);
        }
        navigator.vibrate(50);
        break;
    }
    propagateOrientation();
  }
  window.addEventListener('orientationchange', onScreenOrientationChangeEvent, false);

  /* detect if deviceorientation events are being sent */
  function hasDeviceOrientation() {
    if (event.alpha) {
      window.removeEventListener('deviceorientation', hasDeviceOrientation, false);
      options.deviceOrientation = true;
    }
  }
  window.addEventListener('deviceorientation', hasDeviceOrientation, false);
  if (has.mobile) {
    options.deviceOrientation = true;
  }



  /**
   * on resize send resize event with
   * width, height to all chidlren
   */
  function resize() {
    if (canvas === undefined) {
      return;
    }
    var pixelRatio = devicePixelRatio;

    var width = window.innerWidth;
    var height = window.innerHeight;
    if (current) {
      current.app.resize(width, height, pixelRatio);
    }
    if (optimizer.complete) {
      optimizer.reset();
    }
  }
  window.addEventListener('resize', resize, false);



  /* history management */
  function onPopState(evt) {
    if (!evt.state || !evt.state.slug) {
      exports.onBack();
      return;
    }
    var obj = _.findWhere(apps, {
      slug: evt.state.slug
    });
    statelessLoad(obj);
  }
  window.addEventListener('popstate', onPopState, false);

  /**
   * HACK(maxhawkins): Three.JS assumes vertex attribute array 0 is enabled.
   * The spherical demo disables this attribute after using it. To make Three.JS
   * demos work after spherical demos, reenable this attribute on leave.
   *
   * @param  {WebGLContext} ctx
   */
  function hackResetContext(ctx) {
    if (ctx) {
      ctx.enableVertexAttribArray(0);
      ctx.disable(ctx.CULL_FACE);
    }
  }

  /* changes the current app without modifing the history.state */
  function statelessLoad(obj) {
    if (current) {
      current.app.leave();
    }

    if (glCtx) {
      hackResetContext(glCtx);
    }

    current = obj;
    resize();
    if (current) {
      current.app.enter();
    }
  }


  /* exports */

  exports.open = function() {
    if (has.mobile) {
      if (!isFullscreen()) {
        enterFullscreen();
      }
    }
    loop.stop = false;
    loop();
    if (canvas) {
      canvas.style.display = 'block';
    }
    if (fullyLoaded) {
      load(gallery);
      return Promise.resolve();
    } else {
      return loadAll();
    }
  };

  exports.close = function() {
    loop.stop = true;
    canvas.style.display = 'none';
    statelessLoad(null);
  };


  exports.loadByTitle = function(slug) {
    var obj = _.findWhere(apps, {
      slug: slug
    });
    if (obj) {
      load(obj);
    }
  };

  var load = exports.load = function(obj) {
    if (obj) {
      history.pushState({slug: obj.slug}, '', '');
    }
    statelessLoad(obj);
  };

  exports.registry = function() {
    return registry;
  };

  /**
   * options.factory - factory method which takes a gl context and audio context
   *   and returns a new Promise of a demo implementing render(), leave(),
   *   enter(), resize(width, height), orientationchange(degrees)
   * options.slug - slug name
   * options.preview - preview image
   * options.iOS - works on iOS (default is true)
   */
  exports.register = function(options) {
    registry.push(options);
  };

  exports._filter = null;

  exports.onBack = exports.close;

  return exports;

})(this.Demos || {});

 //monitor
(function() {

  var root = this;
  var previousShinySound = root.ShinySound || {};

  var shinysound, cardboard;

  var ShinySound = root.ShinySound = function(ctxAudio, callback) {

    callback();

    // var up = new THREE.Vector3();
    // var forward = new THREE.Vector3();

    this.update = function() {
    };

  };

  function enter() {
          Demos.close();
          videoPlayer.classList.remove('hidden');

      controls.play();        
  	//window.location = "../eleVR";
    //cardboard.scene.visible = true;
    //shinysound.audioTexture.play();
  }

  function leave() {
    //cardboard.scene.visible = false;
    //shinysound.audioTexture.stop();
  }

  function render() {
    //cardboard.update();
    //shinysound.update();
    //cardboard.render();
  }

  function resize() {
    //Cardboard.prototype.resize.apply(cardboard, arguments);
  }

  var factory = function(gl, audio, options) {

    return new Promise(function(resolve) {
        shinysound = new ShinySound(audio, function() {

          resolve({
            enter: enter,
            leave: leave,
            render: render,
            resize: resize
          });

        });

      });

  };

  Demos.register({
    factory: factory,
    slug: 'video 1',
    preview: './images/preview/video.jpg'
   });
  Demos.register({
    factory: factory,
    slug: 'video 2',
    preview: './images/preview/video2.jpg'
   });
  Demos.register({
    factory: factory,
    slug: 'video 3',
    preview: './images/preview/video3.jpg'
   });

})();

//shoe
(function() {

  var root = this;
  var previousShinySound = root.ShinySound || {};

  var shinysound, cardboard;

  var ShinySound = root.ShinySound = function(ctxAudio, callback) {

    callback();

    // var up = new THREE.Vector3();
    // var forward = new THREE.Vector3();

    this.update = function() {
    };

  };

  function enter() {

  }

  function leave() {
    //cardboard.scene.visible = false;
    //shinysound.audioTexture.stop();
  }

  function render() {
    //cardboard.update();
    //shinysound.update();
    //cardboard.render();
  }

  function resize() {
    //Cardboard.prototype.resize.apply(cardboard, arguments);
  }

  var factory = function(gl, audio, options) {

    return new Promise(function(resolve) {
        shinysound = new ShinySound(audio, function() {

          resolve({
            enter: enter,
            leave: leave,
            render: render,
            resize: resize
          });

        });

      });

  };

  Demos.register({
    factory: factory,
    slug: 'preview 1',
    row: 2,
    preview: './images/adidas/shoe.png'
   });

  Demos.register({
    factory: factory,
    slug: 'preview 2',
    row: 2,
    preview: './images/adidas/shoe.png'
   });

  Demos.register({
    factory: factory,
    slug: 'preview 3',
    row: 2,
    preview: './images/adidas/shoe.png'
   });

})();


var detectScreenHeight = (function(navigator) {
  var info = [
    ['HTC One 801e', 59],
    ['HTC One VX', 57],
    ['Motorola Electrify', 54],
    ['Motorola MOT-XT681', 50],
    ['Nexus 5', 62],
    ['Optimus_Madrid', 57],
    ['SAMSUNG EK-GN120', 60],
    ['SAMSUNG GT-B7810/B7810BUALG1', 55],
    ['SAMSUNG GT-I9070/I9070BULD1', 53],
    ['SAMSUNG GT-I9210/I9210XXLC2', 59],
    ['SAMSUNG GT-S5839i/S5839iBULC1', 50],
    ['SAMSUNG GT-S7500/S7500BULB1', 52],
    ['SAMSUNG GT-S7580', 53],
    ['SAMSUNG SCH-I545', 63],
    ['SAMSUNG-GT-B9388_TD/1.0', 49],
    ['SAMSUNG-SGH-I437', 59],
    ['SAMSUNG-SGH-I537', 63],
    ['SAMSUNG-SGH-I547', 53],
    ['SAMSUNG-SGH-I577', 53],
    ['SAMSUNG-SGH-I727', 60],
    ['SAMSUNG-SGH-I777', 57],
    ['SAMSUNG-SGH-I827', 46],
    ['SAMSUNG-SGH-I847', 49],
    ['SAMSUNG-SGH-I857', 46],
    ['SAMSUNG-SGH-I927', 53],
    ['SAMSUNG-SM-G730A', 53],
    ['SonyEricssonX10i', 50]
  ];

  /**
   * detectScreenHeight returns a device's screen height in millimeters while
   * in landscape orientation based on its user agent string. Its database
   * contains heights for the top Android phones as of June 2014.
   *
   * @param  {String} [userAgent] - phone's user agent string
   * @return {Number} the screen height in millimeters while in landscape, or 53
   * if the device is not in the database.
   */
  return function detectScreenHeight(userAgent) {
    userAgent = userAgent || navigator.userAgent;

    for (var i = 0; i < info.length; i++) {
      var name = info[i][0];
      var height = info[i][1];

      if (userAgent.indexOf(name) > 0) {
        return height;
      }
    }

    return null;
  };

})(navigator);

function detectScreenSize(win) {
  win = win || window;

  var screenHeight = detectScreenHeight();
  if (!screenHeight) {
    return null;
  }

  var resolutionX = win.innerWidth;
  var resolutionY = win.innerHeight;

  var orientation = getOrientation();

  // swap width and height in portrait orientation
  if (orientation === 0 || orientation === 180) {
    var temp = resolutionX;
    resolutionX = resolutionY;
    resolutionY = temp;
  }

  var ratio = resolutionX / resolutionY;
  var screenWidth = screenHeight * ratio;

  return {
    mm: {
      x: screenWidth,
      y: screenHeight
    },
    px: {
      x: resolutionX,
      y: resolutionY
    }
  };
}

var getOrientation = function() {
  switch (window.screen.orientation || window.screen.mozOrientation) {
    case 'landscape-primary':
      return 90;
    case 'landscape-secondary':
      return -90;
    case 'portrait-secondary':
      return 180;
    case 'portrait-primary':
      return 0;
  }
  // if (!window.orientation && window.innerWidth > window.innerHeight)
  //   return 90;
  return window.orientation || 0;
};


// Date.now shim for (ahem) Internet Explo(d|r)er
if ( Date.now === undefined ) {

    Date.now = function () {

        return new Date().valueOf();

    };

}

function Cardboard(gl) {

  this.renderer = new THREE.WebGLRenderer({
    antialias: true,
    canvas: gl.canvas
  });
  this.renderer.setSize(window.innerWidth, window.innerHeight);
  this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
  this.scene = new THREE.Scene();
  this.effect = new THREE.StereoEffect(this.renderer);

  this.camera = new THREE.PerspectiveCamera(
  90, window.innerWidth / window.innerHeight, 1, 500000
  );
  this.scene.add(this.camera);

  this.controls = new THREE.DeviceOrientationControls(this.camera, true);

  this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
  this.orbitControls.noZoom = true;
  this.orbitControls.noPan = true;
  this.orbitControls.autoRotate = false;

  //window.addEventListener('resize', this.resize.bind(this), false);

  //this._initControls = this.initControls.bind(this);
  //window.addEventListener('deviceorientation', this._initControls, false);

  //setTimeout(this.resize.bind(this), 0);

  // hack for resize when iframe doesn't get a rotation event
  // window.addEventListener('message', this.resize.bind(this), false );

  //this.animate = this.animate.bind(this);
  //setTimeout(this.play.bind(this), 0);

}

Cardboard.prototype.initControls = function(event) {
  if (event.alpha) {

    window.removeEventListener('deviceorientation', this._initControls, false);
    this.renderer.domElement.addEventListener('click', this.fullscreen.bind(this), false);

    this.orbitControls.enabled = false;

    this.controls.connect();
    this.controls.update();
  }
};

Cardboard.prototype.animate = function() {
  if (!this._playing) { return; }
  requestAnimationFrame(this.animate);
  this.update();
  this.render();
};

Cardboard.prototype.pause = function() {
  this._playing = false;
};

Cardboard.prototype.play = function() {
  if (this._playing) { return; }
  this._playing = true;
  this.animate();
};

Cardboard.prototype.update = function() {
  // hack to resize if width and height change
  //if (this.width !== window.innerWidth || this.height !== window.innerHeight) {
  //this.resize();
  //}
  this.camera.updateProjectionMatrix();
  if (this.controls.freeze === false) {
    this.controls.update();
  } else {
    this.orbitControls.update();
  }
};

Cardboard.prototype.render = function() {
  //if (this.controls.freeze === false) {
  if (this.currentRenderer) {
    this.currentRenderer.render(this.scene, this.camera);
    return;
  }
  this.effect.render(this.scene, this.camera);
  //} else {
  //this.renderer.render(this.scene, this.camera);
  //}
};

Cardboard.prototype.fullscreen = function() {
  if (this.renderer.domElement.requestFullscreen) {
    this.renderer.domElement.requestFullscreen();
  } else if (this.renderer.domElement.msRequestFullscreen) {
    this.renderer.domElement.msRequestFullscreen();
  } else if (this.renderer.domElement.mozRequestFullScreen) {
    this.renderer.domElement.mozRequestFullScreen();
  } else if (this.renderer.domElement.webkitRequestFullscreen) {
    this.renderer.domElement.webkitRequestFullscreen();
  }
};

Cardboard.prototype.resize = function(width, height, devicePixelRatio) {
  this.width = width;
  this.height = height;
  this.camera.aspect = this.width / this.height;
  this.camera.updateProjectionMatrix();
  if (devicePixelRatio) {
    this.renderer.devicePixelRatio = this.effect.devicePixelRatio = devicePixelRatio;
  }
  this.renderer.setSize(this.width, this.height);
  this.effect.setSize(this.width, this.height);
};

/* exported GalleryLoader */

var GalleryLoader = (function() {

  function GalleryLoader(img) {
    this.container = document.createElement('div');
    this.container.className = 'gallery-loader';

    this.info = document.createElement('div');

	this.heading = document.createElement('h1');
    this.message = document.createElement('h2');
    this.img = img;

    this.info.appendChild(this.heading);
    this.info.appendChild(this.message);
    if (img) {
      this.info.appendChild(this.img);
    }
    this.container.appendChild(this.info);

	this.heading.innerHTML = 'Loading'
    this.setMessage('');
  }

  GalleryLoader.create = function(imgURL) {
    return new Promise(function(resolve) {
      var img = new Image();
      img.onload = function() {
        resolve(new GalleryLoader(img));
      };
      img.onerror = function() {
        resolve(new GalleryLoader(null));
      };
      img.src = imgURL;
    });
  };

  GalleryLoader.prototype.setMessage = function(msg) {
    this.message.innerHTML = msg;
  };

  GalleryLoader.prototype.setProgress = function(done, total) {
    var msg = done + ' of ' + total;
    this.setMessage(msg);
  };

  GalleryLoader.prototype.show = function() {
    this.container.classList.add('enabled');
  };

  GalleryLoader.prototype.hide = function() {
    this.container.classList.remove('enabled');
  };

  return GalleryLoader;

})();

var preview1, preview2, preview3, selectedPreview, deselectedPreview;
var galleryPreview;
var mappingDemo1, mappingDemo2, mappingDemo3;
var tween, detween, objectPos;
var tempGallery, textureShoe1, textureShoe2, textureShoe3, textureBase;

(function(root) {

  var audioCtx;

  // local scale factor to make the ring appear correct
  var _SCALE = 3;

  function Cursor() {
    var geometry = new THREE.RingGeometry(
    0.85 * Cursor.SIZE * _SCALE, 1 * Cursor.SIZE * _SCALE, 32
    );
    var material = new THREE.MeshBasicMaterial(
    {
      color: 0xffffff,
      blending: THREE.AdditiveBlending,
      side: THREE.DoubleSide
    }
    );
    THREE.Mesh.call(this, geometry, material);
  }

  Cursor.SIZE = 0.1 * _SCALE;
  Cursor.prototype = Object.create(THREE.Mesh.prototype);
  Cursor.prototype.constructor = Cursor;

  function Demo(info) {
    THREE.Object3D.call(this);

    this.slug = info.slug;
    this.row = info.row;
    this.href = info.href;
    this.app = info.app;
	
	if (info.slug !== 'preview 1' && info.slug !== 'preview 2' && info.slug !== 'preview 3')
	{
		var geometry = new THREE.PlaneGeometry(this.width, this.height);
		var texture = THREE.ImageUtils.loadTexture(info.preview);
		var material = new THREE.MeshBasicMaterial({
		  transparent: true,
		  map: texture
		});
		var plane = new THREE.Mesh(geometry, material);
		//if (info.slug == 'preview 1') preview1 = plane;
		//if (info.slug == 'preview 2') preview2 = plane;
		//if (info.slug == 'preview 3') preview3 = plane;
		this.add(plane);
	}
  }

  Demo.prototype = Object.create(THREE.Object3D.prototype);
  Demo.prototype.constructor = Demo;
  Demo.prototype.width = 2 * _SCALE;
  Demo.prototype.height = 2 * _SCALE;

  // highlight shows it for selection
  Demo.prototype.highlight = function() {
    this.object.material.color.setHex(0xff0000);
  };

  // begin private gallery setup functions
  function lookToClick() {
    // create the raycaster
    this.projector = new THREE.Projector();
    this.raycaster = new THREE.Raycaster();
  }

  var RADIUS = 5 * _SCALE;

  var resolveSkyBox;
  var skyBoxPromise = new Promise(function(resolve) {
    resolveSkyBox = resolve;
  });

  var skyBoxTexture = THREE.ImageUtils.loadTextureCube([
    './textures/skybox_stars/512/s_px.jpg',
    './textures/skybox_stars/512/s_nx.jpg',
    './textures/skybox_stars/512/s_py.jpg',
    './textures/skybox_stars/512/s_ny.jpg',
    './textures/skybox_stars/512/s_pz.jpg',
    './textures/skybox_stars/512/s_nz.jpg'
  ], undefined, resolveSkyBox);


  var skyBoxShader = THREE.ShaderLib.cube;

  skyBoxShader.uniforms = THREE.UniformsUtils.clone(skyBoxShader.uniforms);
  skyBoxShader.uniforms.tCube.value = skyBoxTexture;

  var skyBoxMaterial = new THREE.ShaderMaterial({
    fragmentShader: skyBoxShader.fragmentShader,
    vertexShader: skyBoxShader.vertexShader,
    uniforms: skyBoxShader.uniforms,
    depthTest: false,
    depthWrite: false,
    side: THREE.BackSide
  });

  var skyBox = new THREE.Mesh(
    new THREE.BoxGeometry(200, 200, 200),
    skyBoxMaterial
  );

  var Param = function(initial, destination, duration) {

    this.i = initial;
    this.d = destination;

    this.value = this.i;
    this.duration = duration || 250;
    this.tween = new TWEEN.Tween(this)
      .to({ value: this.d }, this.duration)
      .easing(TWEEN.Easing.Circular.Out);

  };

  Param.prototype.reset = function() {
    this.value = this.i;
    this.tween.stop();
    return this;
  };

  var Instructions = function(width, height) {

    var scope = this;

    this.canvas = document.createElement('canvas');
    this.canvas.width = width;
    this.canvas.height = height;
    this.ctx = this.canvas.getContext('2d');

    var size = _SCALE * 3;
    var aspect = width / height;

    var geometry = new THREE.PlaneGeometry(size, size / aspect);

    var material = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      map: new THREE.Texture(this.canvas),
      transparent: true,
      depthTest: false
    });

    THREE.Mesh.call(this, geometry, material);

    this.params = {
      nexus5: {
        rotation: new Param(- Math.PI / 2, 0)
      },
      cardboard: {
        opacity: new Param(0, 1),
        y: new Param(this.canvas.height / 3, 0),
        swing: { i: 0, d: Math.PI / 2, value: 0 }
      }
    };

    this.params.nexus5.rotation.tween
      .onStart(function() {
        scope.message = Instructions.IntroMessage;
      });

    this.params.cardboard.opacity.tween
      .delay(this.params.nexus5.rotation.duration / 2);

    this.params.cardboard.y.tween
      .delay(this.params.nexus5.rotation.duration / 2);

    var swing = this.params.cardboard.swing;
    swing.t1 = new TWEEN.Tween(swing)
      .to({ value: swing.d }, 250)
      .onStart(function() {
        scope.message = Instructions.AlertMessage;
      })
      .easing(TWEEN.Easing.Circular.Out)
      .delay(1000)
      .onComplete(function() {
        swing.t2.delay(150).start();
      });

    swing.t2 = new TWEEN.Tween(swing)
      .to({ value: swing.i }, 250)
      .easing(TWEEN.Easing.Sinusoidal.Out)
      .delay(150);

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.material.map.needsUpdate = true;

    this.message = Instructions.IntroMessage;
    this.footnote = Instructions.IntroFootnote;

  };

  Instructions.IntroMessage = ['Turn up volume', 'and insert to begin'];
  Instructions.AlertMessage = ['Rotate phone to return', 'to virtual reality'];

  Instructions.IntroFootnote = 'Having trouble? Set your screen to auto-rotate.';
  Instructions.AlertFootnote = '';

  Instructions.Nexus5 = document.createElement('img');
  Instructions.Cardboard = document.createElement('img');

  Instructions.Nexus5.promise = new Promise(function(resolve, reject) {
    Instructions.Nexus5.onload = resolve;
    Instructions.Nexus5.onerror = reject;
    Instructions.Nexus5.src = './images/gallery/nexus5.png';
  });

  Instructions.Cardboard.promise = new Promise(function(resolve, reject) {
    Instructions.Cardboard.onload = resolve;
    Instructions.Cardboard.onerror = reject;
    Instructions.Cardboard.src = './images/gallery/cardboard_icon.png';
  });

  Instructions.prototype = Object.create(THREE.Mesh.prototype);
  Instructions.prototype.playing = false;

  Instructions.prototype.playIntro = function() {

    if (!this.playing) {
      return this;
    }

    var scope = this;

    this.params.nexus5.rotation
      .reset()
      .tween.start();

    this.params.cardboard.opacity
      .reset()
      .tween.start();

    this.params.cardboard.y
      .reset()
      .tween.start();

    this.swing();

    this.message = Instructions.IntroMessage;
    this.footnote = Instructions.IntroFootnote;

    var swing = this.params.cardboard.swing;

    swing.t2
      .onComplete(function() {
        setTimeout(function() {
          scope.playIntro();
        }, 1000);
      });

    return this;

  };

  Instructions.prototype.playAlert = function() {

    this.params.nexus5.rotation
      .reset()
      .value = this.params.nexus5.rotation.d;

    this.params.cardboard.opacity
      .reset()
      .value = this.params.cardboard.opacity.d;

    this.params.cardboard.y
      .reset()
      .value = this.params.cardboard.y.d;

    this.swing();

    this.message = Instructions.AlertMessage;
    this.footnote = Instructions.AlertFootnote;

    var swing = this.params.cardboard.swing;

    swing.t2
      .onComplete(function() {
        swing.t1.start();
      });

    return this;

  };

  Instructions.prototype.swing = function() {

    var swing = this.params.cardboard.swing;

    swing.t1.stop();
    swing.t2.stop();

    swing.value = swing.i;

    swing.t1.delay(
      this.params.nexus5.rotation.duration +
      this.params.cardboard.opacity.duration +
      1000
    ).start();

    this.playing = true;

    return this;

  };

  Instructions.prototype.update = function() {

    if (!this.playing) {
      return this;
    }

    TWEEN.update();

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    var x, y, width, height;

    width = Instructions.Cardboard.width;
    height = Instructions.Cardboard.height;
    x = this.canvas.width / 2;
    y = this.canvas.height / 2 - this.canvas.height / 5;

    this.ctx.save();
    this.ctx.translate(x + width / 2, y + height / 2);

    this.ctx.save();
    this.ctx.rotate(this.params.cardboard.swing.value);
    this.ctx.translate(- width / 2, - height / 2);

    this.ctx.save();
    this.ctx.rotate(this.params.nexus5.rotation.value);

    width = Instructions.Nexus5.width;
    height = Instructions.Nexus5.height;

    this.ctx.globalAlpha = 1;
    this.ctx.drawImage(Instructions.Nexus5, - width / 2, - height / 2);

    this.ctx.restore();

    width = Instructions.Cardboard.width;
    height = Instructions.Cardboard.height;

    y = this.params.cardboard.y.value;
    this.ctx.globalAlpha = this.params.cardboard.opacity.value;
    this.ctx.drawImage(Instructions.Cardboard, - width / 2, y - height / 2);

    this.ctx.restore();

    this.ctx.restore();

    var lineheight = 24;

    this.ctx.font = 'bold ' + lineheight + 'pt "Roboto", sans-serif';
    this.ctx.textAlign = 'center';
    this.ctx.textBaseline = 'middle';
    this.ctx.fillStyle = '#fff';

    for (var i = 0; i < this.message.length; i++) {
      var message = this.message[i];
      this.ctx.fillText(message, this.canvas.width / 2, this.canvas.height * 0.66 + lineheight * i * 1.5 - lineheight);
    }

    this.ctx.font = (lineheight * 0.75) + 'pt "Roboto", sans-serif';
    this.ctx.fillStyle = '#fff';
    this.ctx.fillText(this.footnote, this.canvas.width / 2, this.canvas.height - lineheight);

    this.material.map.needsUpdate = true;

    return this;

  };

  var instructions = new Instructions(600, 400);
  instructions.position.z = - 50;

  // end private gallery setup functions


  function loadAssets(gallery) {

    return Promise.all([
      skyBoxPromise,
      Instructions.Nexus5.promise,
      Instructions.Cardboard.promise,
      //openSoundPromise
    ]);

  }
  
  function Gallery() {

    THREE.Object3D.call(this);

    lookToClick.bind(this)();

    // placeholder sound function.
    this.openSound = this.selectSound = {
      play: function() {}
    };

    this.intersectables = [];

    for (var i = 0; i < arguments.length; i++) {
      //this.add(arguments[i]);
    }
	tempGallery = this;
	
	var pointLight = new THREE.PointLight(0xFFFFFF, 1);

	// set its position
	pointLight.position.x = 10;
	pointLight.position.y = 50;
	pointLight.position.z = 130;

	// add to the scene
	this.add(pointLight);

	var ambient = new THREE.AmbientLight( 0x101030 );
	this.add( ambient );

	textureBase = new THREE.Texture();
	textureShoe1 = new THREE.Texture();
	textureShoe2 = new THREE.Texture();
	textureShoe3 = new THREE.Texture();
	
	var loader = new THREE.ImageLoader( );
	loader.load( 'models/Ke Dep.png', function ( image ) {
		textureBase.image = image;
		textureBase.needsUpdate = true;
	} );
	
	var loader = new THREE.ImageLoader( );
	loader.load( 'models/Adidas_D_01.png', function ( image ) {
		textureShoe1.image = image;
		textureShoe1.needsUpdate = true;
	} );
	
	var loader = new THREE.ImageLoader( );
	loader.load( 'models/Adidas_D_02.png', function ( image ) {
		textureShoe2.image = image;
		textureShoe2.needsUpdate = true;
	} );
	
	var loader = new THREE.ImageLoader( );
	loader.load( 'models/Adidas_D_03.png', function ( image ) {
		textureShoe3.image = image;
		textureShoe3.needsUpdate = true;
	} );
	
    var loader = new THREE.OBJLoader();
	loader.load( 'models/Room.obj', function ( object ) {
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.z = 200;
		  object.position.y = -140;
	} );
	
	var loader = new THREE.OBJLoader();
	
	loader.load( 'models/Ke Dep.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureBase;
			}
		} );
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.z = 400;
		  object.position.y = -140;
	} );
	loader.load( 'models/Ke Dep.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureBase;
			}
		} );
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.x = 200;
		  object.position.z = 300;
		  object.position.y = -140;
	} );
	loader.load( 'models/Ke Dep.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureBase;
			}
		} );
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.x = -200;
		  object.position.z = 300;
		  object.position.y = -140;
	} );
	
	
	
	loader.load( 'models/Giay.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureShoe1;
			}
		} );
			preview1 = object;
			if (galleryPreview) galleryPreview.intersectables.push(preview1.children[0]);
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.z = 300;
		  object.position.y = -25;
	} );
	
	loader.load( 'models/Giay.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureShoe2;
			}
		} );
			preview2 = object;
			if (galleryPreview) galleryPreview.intersectables.push(preview2.children[0]);
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.x = 200;
		  object.position.z = 300;
		  object.position.y = -25;
	} );
	
	loader.load( 'models/Giay.obj', function ( object ) {
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material.map = textureShoe3;
			}
		} );
			preview3 = object;
			if (galleryPreview) galleryPreview.intersectables.push(preview3.children[0]);
		  tempGallery.add( object );
		  object.scale.x = 0.05;
		  object.scale.y = 0.05;
		  object.scale.z = 0.05;
		  object.rotation.y = 3.14;
		  object.position.x = -200;
		  object.position.z = 300;
		  object.position.y = -25;
	} );
  }


  Gallery.prototype = Object.create(THREE.Object3D.prototype);
  Gallery.prototype.constructor = Gallery;
  
  Gallery.prototype.getIntersectables = function() {
	  return this.intersectables;
  }

  Gallery.prototype.addDemos = function(demos) {
    var hl = 3.0 / 5.0;
    for (var i = 0; i < demos.length; i++) {
      var demo = new Demo(demos[i]);
      var theta = ((i - (demo.row == 2 ? 3 : 0)) * 0.5 + hl);
      demo.position.x = RADIUS * Math.cos(theta + 0.5);
      demo.position.z = RADIUS * Math.sin(theta + 0.5);
      demo.position.y = demo.row == 2 ? -4 : 4;
      demo.lookAt(cardboard.camera.position);
      this.add(demo);
	  if (demo.row !== 2)
		this.intersectables.push(demo.children[0]);
	  else
	  {
		    if (demo.slug == 'preview 1') 
			{
				if (preview1) this.intersectables.push(preview1.children[0]);
				mappingDemo1 = demo;
			}
			if (demo.slug == 'preview 2')
			{
				if (preview2) this.intersectables.push(preview2.children[0]);
				mappingDemo2 = demo;
			}
			if (demo.slug == 'preview 3') 
			{
				if (preview3) this.intersectables.push(preview3.children[0]);
				mappingDemo3 = demo;
			}
	  }
    }
	galleryPreview = this;
  };

  var TTL = 100;
  Gallery.prototype.findIntersections = function() {
    var gaze = new THREE.Vector3(0, 0, 1);

	gaze.unproject(cardboard.camera);
    //this.projector.unprojectVector(gaze, cardboard.camera);

    this.raycaster.set(
      cardboard.camera.position,
      gaze.sub(cardboard.camera.position).normalize()
    );

    var intersects = this.raycaster.intersectObjects(this.intersectables);

    // reset
    this.intersectables.forEach(function(i) {
      if (i != selectedPreview)
      {
		if (i.parent == preview1 || i.parent == preview2 || i.parent == preview3)
		{
			i.scale.set(1, 1, 1);
		}
		else
		{
			i.scale.set(1, 1, 1);
			i.position.z = 0;
		}
      }
    });
    cursor.scale.set(1, 1, 1);

    // if found
    if (intersects.length > 0) {
      var found = intersects[0];
      // highlight
      if (found.object != selectedPreview)
      {
		if (found.object.parent == preview1 || found.object.parent == preview2 || found.object.parent == preview3)
		{
			
			found.object.scale.set(1.2, 1.2, 1.2);
		}
		else
		{
			found.object.scale.set(1.2, 1.2, 1.2);
			found.object.position.z = 0.1;
		}
      }
      if (!this.selected) {
        this.selectSound.play();
        navigator.vibrate(30);
        this.selected = {
          id: found.object.uuid,
          ttl: TTL,
          obj: found.object
        };
      } else {
        if (this.selected.id === found.object.uuid) {
          // decrement
          this.selected.ttl -= 1;
          var p = (this.selected.ttl / TTL);
          cursor.scale.set(p, p, p);
          if (this.selected.ttl <= 0) {
            p = p * 100;
            cursor.scale.set(p, p, p);
			if (this.selected.obj.parent == preview1) this.open(mappingDemo1);
			else if (this.selected.obj.parent == preview2) this.open(mappingDemo2);
			else if (this.selected.obj.parent == preview3) this.open(mappingDemo3);
            else this.open(this.selected.obj.parent);
            // cursor.scale.set(0,0,0);
          }
        } else {
          this.selectSound.play();
          navigator.vibrate(30);
          this.selected = {
            id: found.object.uuid,
            ttl: TTL,
            obj: found.object
          };
        }
      }
    } else {
      this.selected = null;
    }
  };

  Gallery.prototype.open = function(demo) {
    if (this.open.opening) {
      return;
    }
    this.openSound.play();
    navigator.vibrate(100);
    this.open.opening = true;
    if (demo.slug == 'preview 1' || demo.slug == 'preview 2' || demo.slug == 'preview 3')
    {
       deselectedPreview = selectedPreview;
       if (demo.slug == 'preview 1') selectedPreview = preview1;
       if (demo.slug == 'preview 2') selectedPreview = preview2;
       if (demo.slug == 'preview 3') selectedPreview = preview3;

        var objectTarget, deobjectTarget;

        objectPos = {x:selectedPreview.position.x,  y: selectedPreview.position.y, z: selectedPreview.position.z };
		objectTarget = {x: 0, y:-50, z: 80 };

        tween = new TWEEN.Tween(objectPos).to(objectTarget, 500).onUpdate(function(){
            selectedPreview.position.x = objectPos.x;
            selectedPreview.position.y = objectPos.y;
            selectedPreview.position.z = objectPos.z;
        }).start();

if (deselectedPreview != null)
{
        deobjectPos = {x:deselectedPreview.position.x,  y: deselectedPreview.position.y, z: deselectedPreview.position.z };
             if (deselectedPreview == preview1) deobjectTarget = {x: 0, y:-25, z: 300 }
        else if (deselectedPreview == preview2) deobjectTarget = {x: 200, y:-25, z: 300 }
        else if (deselectedPreview == preview3) deobjectTarget = {x: -200, y:-25, z: 300 }

        detween = new TWEEN.Tween(deobjectPos).to(deobjectTarget, 500).onUpdate(function(){
            deselectedPreview.position.x = deobjectPos.x;
            deselectedPreview.position.y = deobjectPos.y;
            deselectedPreview.position.z = deobjectPos.z;
        }).start();
}
      setTimeout(function() {
        this.open.opening = false;
        this.selected = null;
      }.bind(this), 1000);
    }
    else
    {
      setTimeout(function() {
        Demos.load(demo);
        this.open.opening = false;
        this.selected = null;
      }.bind(this), 1000);
    }
    // window.location.href = demo.href;
  };

  Gallery.prototype.update = function(ts, td) {

        TWEEN.update();
    instructions.update(ts, td);

    if (instructions.playing) {
      return;
    }

    this.findIntersections();

  };

  // export
  root.Gallery = Gallery;
  root.Demo = Demo;
  root.Cursor = Cursor;


  // add no sleep

  // add full screen

  // initialize the screen


  var cardboard, gallery, cursor;
  var isPortrait = true;

  function generateGallery(gl, audio, options) {

    var firstTime = true;

    audioCtx = audio;

    cardboard = new Cardboard(gl);

    gallery = new Gallery(skyBox);
    cardboard.scene.add(gallery);

    gallery.showOverlay = showOverlay;
    gallery.hideOverlay = hideOverlay;

    cursor = new Cursor();
    cardboard.camera.add(cursor);
    cardboard.camera.add(instructions);
    cursor.position.z = -3 * _SCALE;
    instructions.position.z = cursor.position.z;
    cursor.lookAt(cardboard.camera.position);

    cardboard.effect.separation = 0.6;

    if (options.deviceOrientation) {
      cardboard.orbitControls.enabled = false;
      cardboard.controls.connect();
    }

    if (!has.mobile) {
      setTimeout(function() {
        cardboard.orbitControls.target.set(0, 0.0, 1);
      }, 0);
    }

    function addDemos(demos) {
      gallery.addDemos(demos);
    }

    function enter() {
      cardboard.scene.visible = true;

      if (has.mobile && isPortrait) {
        showOverlay();
      } else {
        hideOverlay();
      }
    }

    function leave() {
      cardboard.scene.visible = false;
    }

    function render(ts) {
      cardboard.update();
      gallery.update(ts);
      cardboard.render(ts);
    }

    function resize() {
      Cardboard.prototype.resize.apply(cardboard, arguments);
    }

    function hideOverlay() {
      instructions.visible = false;
      instructions.playing = false;

      cursor.visible = true;
      cardboard.currentRenderer = cardboard.effect;
    }

    function showOverlay() {
      instructions.visible = true;
      instructions.playing = true;

      cursor.visible = false;
      cardboard.currentRenderer = cardboard.renderer;

      if (firstTime) {
        instructions.playIntro();
        firstTime = false;
        return;
      }

      instructions.playAlert();

    }

    function orientationchange(orientation) {
      switch (orientation) {
        case -90:
        case 90: // horizontal
          hideOverlay();
          isPortrait = false;
          break;
        case 0: // vertical
        case 180:
          showOverlay();
          isPortrait = true;
          break;
      }
    }

    return loadAssets(gallery).then(function() {
      return {
        enter: enter,
        leave: leave,
        render: render,
        resize: resize,
        orientationchange: orientationchange,
        addDemos: addDemos
      };
    });
  }

  Demos.register({
    factory: generateGallery,
    slug: 'gallery'
  });

})(this);

this.videoPlayer = document.getElementById ( 'video-container' );
this.LandingPage = (function(exports) {
  var APP_MODE = {};
  APP_MODE.NOT_INITIALIZED = 'NOT_INITIALIZED';
  APP_MODE.INTRO = 'INTRO';
  APP_MODE.IN_VR = 'IN_VR';
  APP_MODE.VIDEO = 'VIDEO';
  var currentMode = APP_MODE.NOT_INITIALIZED;

  var FULLSCREEN_STRATEGY = {};
  FULLSCREEN_STRATEGY.NONE = 'NONE';
  FULLSCREEN_STRATEGY.FULLSCREEN_API = 'FULLSCREEN_API';
  FULLSCREEN_STRATEGY.ENCOURAGE_USER_SCROLL = 'ENCOURAGE_USER_SCROLL';
  var fullscreenStrategy = FULLSCREEN_STRATEGY.NONE;

  var landingPage = document.getElementById( 'landing_page' );
  var playbutton = document.getElementById( 'loadDemos' );
  var launchButton = document.getElementById( 'launch_button' );
  var popupWindow = document.getElementById( 'popup_window' );
  var tryAnywayLink = document.getElementById( 'try_anyway' );
  var popupCloseButton = document.getElementById( 'popup_close_button' );
  var downloadCodeButton = document.getElementById( 'download_code_button' );
  var previewCodeButton = document.getElementById( 'code_preview' );
  var scrollDummy = document.getElementById( 'scroll_dummy' );
  var codeBlock = document.getElementById( 'code' );

  var codeMirror = null;

  var isInitialized = false;
  var canRunDemos = false;
  var isUsingPaging = false;
  var isFallback = false;
  var animatedGifToLoad = '';

  function initPage(fallback) {
    isFallback = fallback;

    if (has.mobile) {
      landingPage.className += ' mobile';
      isUsingPaging = false;
      animatedGifToLoad = 'mobile15sec_12fps_256.gif';
    } else {
      landingPage.className += ' desktop';
      isUsingPaging = false;
      animatedGifToLoad = 'desktop15sec_12fps_256.gif';
    }

    // use paging
    if (isUsingPaging) {
      var x = document.getElementsByClassName( 'paging_block' );
      for (var i = 0; i < x.length; i++) {
        x[i].style.display = 'block';
      }

      document.getElementById( 'experiments_page_link' ).addEventListener( 'click', gotoExperimentsPage, true );
      document.getElementById( 'dev_page_link' ).addEventListener( 'click', gotoDevPage, true );

      gotoExperimentsPage( null );
    }

    testCapabilities();

    setFullscreenStrategy();

    //loadCodePreview();

    // bind clicks
    if (canRunDemos) {
      //if desktop {
      if (!has.mobile) {
		// show a "are you sure?" pop up
        launchButton.addEventListener( 'click', onOpenPopupClicked, true );
        popupCloseButton.addEventListener( 'click', onClosePopupClicked, true );
        tryAnywayLink.addEventListener( 'click', onLaunchClicked, true );
      } else {
		// launch experiment
        launchButton.addEventListener( 'click', onLaunchClicked, true );

		doSampleAutoRotate = true;
      }
    }

    downloadCodeButton.addEventListener( 'click', onDownloadCodeClicked, true );
    previewCodeButton.addEventListener( 'click', onPreviewCodeClicked, true );

	// wait a beat before loading animated gifs
	setTimeout(loadAnimatedGifs, 1000);

	isInitialized = true;

    startMode( APP_MODE.IN_VR );
  }



  function setFullscreenStrategy() {
    // default to none
    fullscreenStrategy = FULLSCREEN_STRATEGY.NONE;

    if (has.mobile) {
      // if we can go fullscreen
      if (Modernizr.fullscreen) {
        fullscreenStrategy = FULLSCREEN_STRATEGY.FULLSCREEN_API;
      } else {
        fullscreenStrategy = FULLSCREEN_STRATEGY.ENCOURAGE_USER_SCROLL;
      }
    }
  }

  function loadAnimatedGifs() {

      // Load the animated GIF
      var els = document.getElementsByClassName( 'animated_gif_target' );
      for (var i = 0; i < els.length; i++) {
        els[i].style['background-image'] = 'url(\'./images/landingPage/' + animatedGifToLoad + '\')';
      }
  }

  function startMode(newMode) {
    if (currentMode != newMode) {
      var oldMode = currentMode;
      currentMode = newMode;

      if (currentMode == APP_MODE.INTRO) {

		// start the threeJS code example
	    var doSampleAutoRotate = (!has.mobile);
		  CodeExample.start(doSampleAutoRotate);

        // make the scroll dummy small, so it doesn't interfer with normal scrolling
        scrollDummy.style.height = '0px';

        landingPage.style.visibility = 'visible';

        // Go to the top, in case the browser tries to start us further down (on reload, back button, etc)
        moveScrollToTheTopOfPage();
      }
      // we just hit the button to start VR mode
      else if (currentMode == APP_MODE.IN_VR) {

        // stop the threeJS code example

		if (isFallback) {
			startIFrameGallery();
		} else {

          videoPlayer.classList.add('hidden');
	        landingPage.style.visibility = 'hidden';

	        // if we can go fullscreen, do it
	        if (fullscreenStrategy == FULLSCREEN_STRATEGY.FULLSCREEN_API) {
	          fullscreen( document.body );
	        }

	        // make the scroll dummy huge, so we can always scroll down
	        scrollDummy.style.height = '9000px';

	        // start the gallery
	        Demos.open();
		}

      }
      else if (currentMode == APP_MODE.VIDEO) {
          Demos.close();
          videoPlayer.classList.remove('hidden');
      }
    }
  }
  function onLaunchClicked(ev) {
    if (ev) ev.preventDefault();
    startMode( APP_MODE.IN_VR );
  }
  function onDownloadCodeClicked() {
    window.location = 'http://vr.chromeexperiments.com/cardboard.zip';
  }

  function onOpenPopupClicked(ev) {
    ev.preventDefault();
    popupWindow.style.display = 'block';
  }
  function onClosePopupClicked(ev) {
    ev.preventDefault();
    popupWindow.style.display = 'none';
  }


  function onPreviewCodeClicked(ev) {
    ev.preventDefault();

    // toggle code preview
    if (codeBlock.style.display == '') {
      codeBlock.style.display = 'none';
      previewCodeButton.innerHTML = '+Code Preview';
    } else {
      codeBlock.style.display = '';
      previewCodeButton.innerHTML = '-Code Preview';
    }

  }
  function loadCodePreview() {
    var codePreviewElem = document.getElementById( 'code' );
    codeMirror = CodeMirror( codePreviewElem, {
      mode: 'htmlmixed',
      lineWrapping: true,
      readOnly: true,
      theme: 'example'
    } );

    request = new XMLHttpRequest();
    request.open( 'GET', '/cardboard/index.html', true );

    CodeMirror.on( codeMirror.getDoc(), 'change', onCodeMirrorLoaded );

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        // Success!
        //data = JSON.parse(request.responseText);
        var data = request.responseText;
        codeMirror.getDoc().setValue( data );
      } else {
        onCodePreviewLoadError();
      }
    };

    request.onerror = function() {
      onCodePreviewLoadError();
    };

    request.send();

  }
  function onCodeMirrorLoaded() {
    // if we don't wait until it's loaded to hide it, CodeMirror won'd make the textarea the right size
    codeBlock.style.display = 'none';
  }
  function onCodePreviewLoadError() {
    //hide preview button
    document.getElementById( 'developer_code_preview_block' ).style.display = 'none';
  }

  function moveScrollToTheTopOfPage() {
    //setTimeout(doScroll, 200)
    window.scrollTo( 0, 0 );

    /*
    var startTopPos = window.pageYOffset;
    if (startTopPos >= 0) {
    	// move to the top in case they are refreshing the page
    	// so they're forced to scroll down (and hide the chrome)
    	//window.scrollTop = -1; // set to -1, because 0 doesn't seem to do anything in Safari
    	//TODO: WHY ISN'T this working?
    	//window.scroll(0,-1)
    	window.scrollTo(0,0)
    }*/
  }

  function testCapabilities() {
    var cannotPlayMessage = '';
    var missingFeatures = getMissingFeatures();

    // desktop
    if (!has.mobile && has.webgl) {
      showFullPlay();
    }
    // mobile firefox
    else if (has.mobile && has.Firefox) {
      showLimitedPlay( 'This experience doesn’t support your browser (yet). Try anyway?' );
    }
    // mobile, but missing required features
    else if (missingFeatures != '') {
      //showCannotPlay( 'Sorry, these experiments will not run on this device because they require ' + missingFeatures );
      showCannotPlay( 'This experience requires a WebGL-enabled mobile browser, such as Google Chrome.' );
    } else {
      showFullPlay();
    }
  }

  function showFullPlay() {
    canRunDemos = true;
  }
  function showLimitedPlay(msg) {
    var warningEl = document.getElementById( 'warning_message' );
    warningEl.innerHTML = msg;
    warningEl.style.display = 'block';

    canRunDemos = true;
  }
  function showCannotPlay(msg) {
    var errorEl = document.getElementById( 'error_message' );
    errorEl.innerHTML = msg;
    errorEl.style.display = 'block';

    launchButton.className += ' ' + 'disabled';

    // hide ThreeJS code example
    if (!has.webgl) {
    	document.getElementById( 'developer_example' ).style.display = 'none';
	}

    canRunDemos = false;
  }


  function getMissingFeatures() {
    if (!has.webgl) return 'WebGL';
    if (!Modernizr.webaudio) return 'Web Audio API';
    if (!Modernizr.canvas) return 'Canvas';
    if (!Modernizr.devicemotion ||
      !Modernizr.deviceorientation) return 'gyroscope';
    // if (!Modernizr.fullscreen) return 'Full Screen API';
    if (!Modernizr.history ||
      !Modernizr.postmessage) return 'HTML5';
    if (!Modernizr.touch) return 'touch';
    return '';
  }

  function fullscreen(target) {
    if (target.requestFullscreen) {
      target.requestFullscreen();
    } else if (target.msRequestFullscreen) {
      target.msRequestFullscreen();
    } else if (target.mozRequestFullScreen) {
      target.mozRequestFullScreen();
    } else if (target.webkitRequestFullscreen) {
      target.webkitRequestFullscreen();
    }
  }

  // Paging Functions
  function gotoDevPage(ev) {
    if (ev) {
      ev.preventDefault();
    }
    gotoPage( 'dev' );
  }
  function gotoExperimentsPage(ev) {
    if (ev) {
      ev.preventDefault();
    }
    gotoPage( 'experiments' );
  }
  function gotoPage(pagePrefix) {
    // hide all pages
    var x = document.getElementsByClassName( 'page' );
    for (var i = 0; i < x.length; i++) {
      x[i].style.display = 'none';
    }

    // show this one
    document.getElementById( pagePrefix + '_page' ).style.display = 'block';

    moveScrollToTheTopOfPage();
  }

  exports.start = function(isFallback) {
    if (!isInitialized) {
       initPage(isFallback);
    } else {
       startMode( APP_MODE.INTRO );
    }
  };

  // extend close to also start the page
  Demos.onBack = function() {
    //Demos.close();
    //exports.start(isFallback);
  };

  return exports;
})( this.LandingPage || {} );

// IE console.log fix
if (!window.console) {var console = {};}
if (!console.log) {console.log = function() {};}

LandingPage.start(false);

